# -*- coding: utf-8 -*-  
__author__ = 'ozgur'
__creation_date__ = '16.04.2019' '15:15'


class ModuleInfo:
    def __init__(self, initialdict: dict = None):
        if not initialdict:
            initialdict = {}

        self.modname = initialdict.get("modname", None)
        self.menuiconclass = initialdict.get("menuiconclass", "fa-modx")  # font awesome icon class
        self.menusuburl = initialdict.get("menusuburl", "/home")


DHM_MODULES = {
    "SysInfo": ModuleInfo({
        'modname': "SysInfo",
        "menuiconclass": "fa-info-circle",
        "menusuburl": "/sysinfo/home",
    }),
    "WebAppManager": ModuleInfo({
        'modname': "WebAppManager",
        "menuiconclass": " fa-files-o",
        "menusuburl": "/wam/walist",
    }),
}
