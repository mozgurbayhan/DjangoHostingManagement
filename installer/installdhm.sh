#!/usr/bin/env bash
# AUTHOR : 'ozgur'
# CREATION DATE : '13.04.2019' '23:51'

SCRIPT_NAME='installdhm.sh'

FILE_SSHD=/etc/ssh/sshd_config
FOLDER_DHM=/root/dhm
FOLDER_DJANGO_HOSTING_MANAGER=${FOLDER_DHM}/DjangoHostingManagement
FOLDER_ITEMPLATES=${FOLDER_DJANGO_HOSTING_MANAGER}/installer/templates
DHM_PIP=${FOLDER_DHM}/venv3/bin/pip3
DHM_PYTHON=${FOLDER_DHM}/venv3/bin/python3


color_end="\033[0m"
echosection(){
    printf "\e[0;32m >\e[0m \e[1;93m ${1} \e[0m\n"
}

echoinfo(){
    printf "\e[0;32m >>>\e[0m \e[1;96m ${1} \e[0m\n"
}

echowarning(){
    printf "\e[0;32m >>>\e[0m \e[1;91m ${1} \e[0m\n"
}

makemysql(){
    echosection "Installing and configuring mysql..."
    apt-get -y install mysql-server

    echoinfo "Securing mysql..."
    echoinfo "We strongly suggest you to answer Y to all after password"
    mysql_secure_installation
    systemctl restart mysql
}

makesudoers(){
    echosection "Installing and configuring sudo..."
    apt-get -y install sudo

}

makessh(){
    echosection "Installing and configuring ssh..."
    apt-get -y install openssh-server
    sed -i 's/Port 22/Port 2222/g' ${FILE_SSHD}
    sed -i 's/PermitRootLogin prohibit-password/PermitRootLogin no/g' ${FILE_SSHD}
    sed -i 's/#Port 2222/Port 2222/g' ${FILE_SSHD}
    sed -i 's/#PermitRootLogin no/PermitRootLogin no/g' ${FILE_SSHD}
    systemctl restart sshd

}

makeufw(){
    echosection "Installing and configuring ufw..."
    apt-get -y install ufw
    echoinfo "Configuring ufw..."
    ufw enable
    ufw default allow outgoing
    ufw default deny incoming
    ufw allow 2222
    ufw allow 3512
    ufw allow http
    ufw allow https
    ufw reload
}

makedenyhosts(){
    echosection "Installing and configuring denyhosts..."
    apt-get -y install denyhosts
    cp -f ${FOLDER_ITEMPLATES}/denyhosts.conf /etc/denyhosts.conf
    systemctl restart denyhosts
}

makenginx(){
    echosection "Installing and configuring nginx..."
    apt-get -y install nginx
    systemctl restart nginx
}

makedhm(){
    echosection "Configuring DHM..."
    cp -f ${FOLDER_ITEMPLATES}/local_settings_template.py ${FOLDER_DJANGO_HOSTING_MANAGER}/DjangoHostingManagement/
    cp -f ${FOLDER_ITEMPLATES}/dhm /usr/bin/dhm
    cp -f ${FOLDER_ITEMPLATES}/.bashrc /root/
    cd ${FOLDER_DJANGO_HOSTING_MANAGER}/
    ${DHM_PYTHON} manage.py migrate
#    echo "from django.contrib.auth.models import User; User.objects.create_superuser('dhmadmin', 'admin@example.com', 'dhmadmin')" | ${DHM_PYTHON} manage.py shell
cat <<EOF | ${DHM_PYTHON} manage.py shell
from django.contrib.auth import get_user_model

User = get_user_model()

User.objects.filter(username="admin").exists() or \
    User.objects.create_superuser("admin", "admin@example.com", "pass")
EOF
    cd -
}

install(){
    cd /root
    echosection "Upgrading packages..."
    apt-get update
    apt-get -y upgrade
    echosection "Installing necessary packages..."
    apt-get -y install python3 python3-venv python3-dev build-essential git python3-virtualenv virtualenv python3-pip  libmysqlclient-dev

    echosection "Creating webapps user..."
    echoinfo "Please fill password and leave rest empty"
    adduser webapps

    echosection "Creating necessary folders..."
    sudo -u webapps mkdir /home/webapps/logs
    sudo -u webapps mkdir /home/webapps/apps
    sudo -u webapps mkdir /home/webapps/venvs
    sudo -u webapps mkdir /home/webapps/backups
    sudo -u webapps mkdir /home/webapps/backups/apps/

    mkdir ${FOLDER_DHM}/

    makemysql
    makesudoers
    makessh
    makenginx

    cd ${FOLDER_DHM}
    echosection "Creating virtual environment..."
    rm -rf venv3
    python3 -m venv venv3

    echosection "Cloning DHM..."
    rm -rf DjangoHostingManagement
    git clone https://gitlab.com/mozgurbayhan/DjangoHostingManagement.git

    echosection "Installing python requirements..."
    ${DHM_PIP} install -r ${FOLDER_DJANGO_HOSTING_MANAGER}/requirements.txt
    echoinfo "Cleaning pip cache..."


    makeufw
    makedenyhosts
    makedhm
}

if [ $(ps -p $$ | awk '$1 == PP {print $4}' PP=$$) != ${SCRIPT_NAME} ] ;then
    echo "Please run this script as ./${SCRIPT_NAME}"
    exit 1
elif [ $EUID -ne 0 ]; then
    echowarning "This script must be run as root!"
    exit 1
else
    echosection "DHM installer version 1.0"
    echowarning "This script will change your ssh port to 2222 and forbid root login via ssh!"
    read -p "Do you accept (y/n)?" choice
    case "$choice" in
      y|Y ) install;;
      n|N ) echo "Goodbye!";;
      * ) echo "Goodbye!";;
    esac
fi
