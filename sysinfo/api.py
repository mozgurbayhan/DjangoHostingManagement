# coding: utf-8
import time
from collections import deque

import psutil
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse

__author__ = 'Sencer HAMARAT'
__creation_date__ = '08.07.2019' '22:12'

symbols = ('K', 'M', 'G', 'T', 'P', 'E', 'Z', 'Y')
byte_prefix = {}
bit_prefix = {}
for i, s in enumerate(symbols):
    byte_prefix[s] = 1 << (i + 1) * 10
    bit_prefix[s] = 1 << i * 10


def bytes2human(n):
    for sym in reversed(symbols):
        if n >= byte_prefix[sym]:
            value = float(n) / byte_prefix[sym]
            return '%.1f%sB' % (value, sym)
    return "%sB" % n


def bits2human(n):
    for sym in reversed(symbols):
        if n >= bit_prefix[sym]:
            value = float(n) / bit_prefix['G']  # Fixed to Gbips according to general network speed
            return '%.1f' % value
    return "%s" % n


def avg_cpu_temp(temp):
    core_temps = temp['coretemp']
    total_temp = 0
    count = len(core_temps)
    for item in core_temps:
        total_temp += item.current
    return total_temp / count


def process_count_percent(pid_count):
    return 65535 / (pid_count * 100)


class NetUsage:

    def __init__(self):
        self.prev_time = time.time()
        self.prev_total = (0, 0)
        self.prev_ul_dl = (0, 0)

    def calc_ul_dl(self, net_io_counters, rate):
        total = (net_io_counters.bytes_sent, net_io_counters.bytes_recv)

        this_time = time.time()
        ul, dl = [(now - last) / (this_time - self.prev_time) for now, last in zip(total, self.prev_total)]

        self.prev_ul_dl = ul, dl
        self.prev_time = time.time()
        self.prev_total = total
        return ul, dl


net_usage = NetUsage()


@login_required
def sys_status(request):
    vm = psutil.virtual_memory()
    disk = psutil.disk_usage("/")
    swap = psutil.swap_memory()
    temp = psutil.sensors_temperatures(fahrenheit=False)
    pids = len(psutil.pids())
    transfer_rate = deque(maxlen=1)
    iotimes = psutil.cpu_times_percent()._asdict()
    net_io_counters = psutil.net_io_counters(pernic=False)
    upload, download = net_usage.calc_ul_dl(net_io_counters, transfer_rate)

    retval = {
        "cpucount": psutil.cpu_count(),
        "cpupercent": int(psutil.cpu_percent()),
        "load_avg": repr(psutil.getloadavg()),
        "memtotal": bytes2human(vm.total),
        "memfree": bytes2human(vm.available),
        "mempercent": int(vm.percent),
        "disktotal": bytes2human(disk.total),
        "diskfree": bytes2human(disk.free),
        "diskpercent": int(disk.percent),
        "swaptotal": bytes2human(swap.total),
        "swapfree": bytes2human(swap.free),
        "swappercent": int(swap.percent),
        "avgcputemp": int(avg_cpu_temp(temp)),
        "pidcount": int(pids),
        "pidcountpercent": int(process_count_percent(pids)),
        "iorate": int(iotimes['iowait'] + iotimes['user'] + iotimes['system']),
        "ioratepercent": int(iotimes['iowait'] + iotimes['user'] + iotimes['system']),
        "ioidlerate": int(iotimes['idle']),
        "ioidleratepercent": int(iotimes['idle']),
        "netsent": bytes2human(net_io_counters.bytes_sent),
        "netsentspeed": bits2human(upload),
        "netsentspeedhr": str(bytes2human(upload)),
        "netreceived": bytes2human(net_io_counters.bytes_recv),
        "netreceivedspeed": bits2human(download),
        "netreceivedspeedhr": str(bytes2human(download)),
    }
    return JsonResponse(retval, safe=False)
