# coding: utf-8
__author__ = 'Sencer HAMARAT'
__creation_date__ = '18.06.2019' '22:21'

from core.config import BaseConfig


class SysInfoConfig(BaseConfig):
    def __init__(self):
        super().__init__("sysinfo")


dc = SysInfoConfig()
dc.load()
dc.save()
