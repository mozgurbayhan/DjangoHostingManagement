# coding: utf-8
import distro
import json
import platform

from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.urls import reverse

from cpuinfo import get_cpu_info_json

__author__ = 'Sencer HAMARAT'
__creation_date__ = '18.06.2019' '22:12'

@login_required
def index(request):

    platform_info = {
        "Node Name": platform.node(),
        "Machine Type": platform.machine(),
        "OS Type": platform.system(),
        "OS Architecture": " ".join(platform.architecture()),
        "Distribution": " ".join(distro.linux_distribution(full_distribution_name=True)).title(),
        "Libc Version": " ".join(platform.libc_ver()),
    }

    register_list = ['flags', 'hz_advertised_raw', 'hz_actual_raw', 'l1_instruction_cache_size', 'l2_cache_line_size',
                     'l2_cache_associativity', 'extended_model', 'python_version']

    content = {
        "platform": platform_info,
        "cpu": json.loads(get_cpu_info_json()),
        "register_list": register_list,
    }
    return render(request, "sysinfo/home.html", content)
