# coding: utf-8
__author__ = 'Sencer HAMARAT'
__creation_date__ = '18.06.2019' '22:12'

from django.apps import AppConfig


class SysinfoConfig(AppConfig):
    name = 'sysinfo'
