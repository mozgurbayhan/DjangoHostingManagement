from django import template

register = template.Library()


@register.filter(name='to_space')
def to_space(value, arg):
    return value.replace(arg, ' ')


@register.filter(name='value_type')
def value_type(value):
    return type(value)
