# coding: utf-8
from django.urls import path

from sysinfo import views, api

__author__ = 'Sencer HAMARAT'
__creation_date__ = '18.06.2019' '22:38'

urlpatterns = [

    path('home/', views.index, name='index'),
    path('sysstatus/', api.sys_status, name='sys_status'),

]
