# DHM - Django Hosting Management


## What is DHM?
DHM is a bundle of django setup and management scripts for linux server. DHM aims on simplicity.

Setup script turns your linux server into a django server and after that you can  manage your django applications in an easy way.

## Status
**Still under development! Not suitable for production yet!**

## Links

- You may follow development process in [here](https://tree.taiga.io/project/obayhan-dhm/timeline) .
- You may report bugs and request your wishes in [here](https://tree.taiga.io/project/obayhan-dhm/issues) .
- You may found our wiki and documentation in [here](https://gitlab.com/mozgurbayhan/DjangoHostingManagement/wikis/DHM)  .

## Supported Operating Systems

- ubuntu 18.04

## Guides
- You may reach documentation base in [here](https://gitlab.com/mozgurbayhan/DjangoHostingManagement/wikis/DHM).
- You may read documentation for developers in [here](https://gitlab.com/mozgurbayhan/DjangoHostingManagement/wikis/DevelopersGuide).
- You may read English User's Guide in [here](https://gitlab.com/mozgurbayhan/DjangoHostingManagement/wikis/UsersGuideEn).
- Türkçe Kullanıcı Dökümanı'na [buradan](https://gitlab.com/mozgurbayhan/DjangoHostingManagement/wikis/UsersGuideTr)  erişebilirsiniz.

## Things that we use
| * | URL |
|--------------|------------------------------------------------------------------------------------------------------|
| python 3     | [https://www.python.org/](https://www.python.org/)                                                   |
| bash         | [https://www.gnu.org/software/bash/](https://www.gnu.org/software/bash/)                             |
| Project Icon | [www.flaticon.com](https://www.flaticon.com/free-icon/server_138960#term=hosting&page=1&position=47) |
| django|  [https://www.djangoproject.com/](https://www.djangoproject.com/)  |
|Taiga.io | [https://taiga.io/](https://taiga.io/) |
|Admin LTE2 | [https://adminlte.io/](https://adminlte.io/) |

## Screen Shots

![apps.img](https://gitlab.com/mozgurbayhan/DjangoHostingManagement/wikis/img/apps.png "Apps Page")
