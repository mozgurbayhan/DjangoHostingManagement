# -*- coding: utf-8 -*-
from django import template
from django.utils.safestring import mark_safe

__author__ = 'ozgur'
__creation_date__ = '28.06.2019' '13:43'

register = template.Library()


@register.filter(name='thread_info', is_safe=True)
def thread_info(tid):
    return mark_safe(TINFOTEMPLATE.replace("{{ tid }}", str(tid)))


TINFOTEMPLATE = '''<div class="ticontainer" id="ticontainer_{{ tid }}">
    <div class="row">
        <div class="col-lg-10">
        <div class="progress progress-sm active">
            <div id="tprogress_{{ tid }}" class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="1" style="width: 00%">

            </div>
        </div>
        </div>
        <div class="col-lg-2 text-purple" id="tprogressinfo_{{ tid }}" >
        </div>
    </div>    
    <hr>
    <div class="tilogcontainer" id="tilogcontainer_{{ tid }}">
    </div>
</div>
<script type="text/javascript">
    function gettinfo(){
        console.log("Requesting");
        $.post({
            url: '/btm/ati/',
            data: {
                'tid': '{{ tid }}'
            },
            dataType: 'json',
            success: function(data){
                if(data.success===true){
                    console.log(data);
                    $("#tilogcontainer_{{ tid }}").html(data.htmllog);
                    var tprogress = $("#tprogress_{{ tid }}");
                    var tprogressinfo = $("#tprogressinfo_{{ tid }}");
                    tprogress.attr("aria-valuenow", data.stepdone);
                    tprogress.attr("aria-valuemax", data.stepcount);
                    tprogress.css("width", data.steppercent.toString() + "%");
                    if(data.isfinished){
                        tprogress.attr("aria-valuenow", data.stepcount);
                        tprogress.attr("aria-valuemax", data.stepcount);
                        if (data.hasfault){
                            tprogressinfo.html("Finished with errors!");
                        }
                        else{
                            tprogressinfo.html("Succesfully finished!");
                        }
                    } else{

                        var pinfotxt="Processing : "+data.stepdone.toString() + " / " + data.stepcount.toString();
                        tprogressinfo.html(pinfotxt);
                        setTimeout(gettinfo, 2000);
                    }

                }
                else{
                    console.log("Unsuccess data");
                    console.log(data);
                    setTimeout(gettinfo, 2000);
                }
            }
        });
    }

    console.log("Timer Set");
    setTimeout(gettinfo, 2000);
</script>'''
