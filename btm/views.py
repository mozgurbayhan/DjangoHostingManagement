# -*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt

from btm.backend_thread_manager import BackendThread
from btm.models import BackgroundThreadInfo

__author__ = 'ozgur'
__creation_date__ = '23.04.2019' '23:01'


@login_required
@csrf_exempt
def ajax_thread_info(request):
    if request.method == 'POST' and "tid" in request.POST:
        try:
            bti = BackgroundThreadInfo.objects.get(id=int(request.POST["tid"]))

            retdict = {
                "success": True,
                "isfinished": bti.isfinished,
                "hasfault": bti.hasfault,
                "stepcount": bti.stepcount,
                "stepdone": bti.stepdone,
                "steppercent": int(bti.stepdone / bti.stepcount * 100),
                "htmllog": bti.get_html_log()
            }

            if bti.isfinished:
                bti.delete()
        except BackgroundThreadInfo.DoesNotExist:

            retdict = {
                "success": False
            }
    else:
        retdict = {
            "success": False
        }

    return JsonResponse(retdict)


@login_required
def getthreadinfo(request, tid):
    return render(request, "btm/getthreadinfo.html", {"tid": tid})


class TestThread(BackendThread):
    def __init__(self):
        super().__init__(stepcount=5)

    def backendthread(self):
        self.bt_logheader("Test Process Started")

        self.bt_logheader("Testing warning")
        self.bt_logwarn("I am a warning (:")
        self.bt_logheader("Testing exception")
        try:
            raise Exception("I am a test exception")
        except Exception as ex:
            self.bt_logexception(ex,"Test Exception raised")
        self.bt_raisestep()

        self.bt_logheader("Testing shell")
        self.bt_execsys('ls -alh | grep "a"', ischained=True)
        self.bt_raisestep()
        self.bt_logheader("Testing Long running process")
        for x in range(0,3):
            self.bt_loginfo("sleeping 10 seconds on shell")
            self.bt_execsys("sleep 10",ischained=True)
            self.bt_raisestep()






@login_required
def testthreadinfo(request):
    tid = None
    isinpage = None
    if request.method == 'POST':
        tth = TestThread()
        tth.start()
        tid = str(tth.get_thread_id())
        if "testinpage" in request.POST:
            isinpage = True
        elif "testoutpage" in request.POST:
            isinpage = False

    return render(request, "btm/testthreadinfo.html", {"tid": tid,"isinpage":isinpage})
