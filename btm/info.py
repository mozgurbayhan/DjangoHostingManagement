# -*- coding: utf-8 -*-  
__author__ = 'ozgur'
__creation_date__ = '20.06.2019' '14:12'


class BTLogState:
    header = "text-green"
    info = "text-primary"
    warn = "text-orange"
    exception = "text-red"
    sysout = "text-purple"
