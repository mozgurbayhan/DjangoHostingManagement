# -*- coding: utf-8 -*-
from django.contrib import admin

from btm.models import BackgroundThreadInfo

__author__ = 'ozgur'
__creation_date__ = '28.06.2019' '15:35'

@admin.register(BackgroundThreadInfo)
class ConfigdbAdmin(admin.ModelAdmin):
    list_display = [
        "id",
        "isfinished",
        "hasfault",
        "stepcount",
        "stepdone",
        "created_at",
        "updated_at",
    ]
