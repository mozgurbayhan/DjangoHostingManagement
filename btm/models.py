# -*- coding: utf-8 -*-

__author__ = 'ozgur'
__creation_date__ = '23.04.2019' '23:01'

from django.db import models


# Create your models here.

class BackgroundThreadInfo(models.Model):
    log = models.TextField(default="", verbose_name="Log")
    isfinished = models.BooleanField(default=False, verbose_name="Is Fınıshed")
    hasfault = models.BooleanField(default=False, verbose_name="Has Fault")
    stepcount = models.IntegerField(default=1, verbose_name="Step Count")
    stepdone = models.IntegerField(default=0, verbose_name="Step Done")

    created_at = models.DateTimeField(auto_now_add=True, verbose_name="Created At")
    updated_at = models.DateTimeField(auto_now=True, verbose_name="Updated At")

    class Meta:
        verbose_name = 'Background Thread'
        verbose_name_plural = 'Background Threads'
        ordering = ('id',)

    def __str__(self):
        return str(self.id)

    def get_short_name(self):
        return str(self.id)

    def add_log(self, text: str) -> None:
        self.log += text
        self.save()

    def raisestep(self, rcount: int = 1):
        if self.stepdone + rcount <= self.stepcount:
            self.stepdone += rcount
        elif self.stepdone < self.stepcount:
            self.stepdone = self.stepcount
        else:
            pass
        self.save()

    def finished(self, issuccess: bool = True):
        self.isfinished = True
        if not issuccess:
            self.hasfault = True
        self.save()

    def get_html_log(self) -> str:
        return self.log
