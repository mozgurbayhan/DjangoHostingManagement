# -*- coding: utf-8 -*-
import traceback
import uuid
from threading import Thread
# noinspection PyPackageRequirements
from time import sleep

import delegator

from btm.info import BTLogState
from btm.models import BackgroundThreadInfo

__author__ = 'ozgur'
__creation_date__ = '23.04.2019' '23:11'


class BackendThread(Thread):
    def __init__(self, stepcount: int = 0):
        """
        Basic thread structure and its related functions for DHM thread management

        :param stepcount: total step count in process
        """
        super().__init__()
        self.bti = BackgroundThreadInfo()
        self.bti.stepcount = stepcount

    def backendthread(self):
        """
        Your main code runs in here. OVERRIDE ME !!!!

        :return:
        """
        pass

    def run(self):
        """
        DON'T call it in our code or override, This is not for nebies (:

        :return:
        """
        self.bti.save()
        try:
            self.backendthread()
        except Exception as ex:
            self.bt_logexception(ex)
        if not self.bti.isfinished:
            self.bti.finished()

    def bt_logheader(self, text: str):
        """
        logs header

        :param text:
        :return:
        """
        tpass = '<br><span class="{}"> >> {}</span>'.format(BTLogState.header,
                                                          text.replace("\n", "br"))
        self.bti.add_log(tpass)

    def bt_loginfo(self, text: str):
        """
        logs info

        :param text:
        :return:
        """
        tpass = '<br><span class="{}"> >> {}</span>'.format(BTLogState.info, text.replace("\n", "br"))
        self.bti.add_log(tpass)

    def bt_logwarn(self, text: str):
        """
        logs exception

        :param text:
        :return:
        """
        tpass = '<br><span class="{}"> >> {}</span>'.format(BTLogState.warn, text.replace("\n", "br"))
        self.bti.add_log(tpass)

    def bt_logexception(self, exc: Exception, infotext: str = None):
        """
        logs exception and its details

        :param exc: exception
        :param infotext: additional info text
        :return:
        """
        if not infotext:
            infotext = "Exception Occured:"

        twarn = '<br><span class="{}"> >> {} : {}</span>'.format(BTLogState.warn, infotext,
                                                               str(exc))
        self.bti.add_log(twarn)

        texc = '<br><span class="{}"> >> {}</span>'.format(BTLogState.exception, "".join(
            traceback.format_tb(exc.__traceback__)).replace("\n", "<br>"))
        self.bti.add_log(texc)

    def bt_execsys(self, command: str, timeout: int = 60, ischained: bool = False) -> int:
        """
        Executes a command and does related operations

        :param command: string representation of command
        :param timeout: terminates after timeout second . Default 60.
        :param ischained: defines is pipeline etc used in command or single line of command
        :return: return state of command
        """
        self.bt_loginfo("Executing: " + command)

        if ischained:
            comm = delegator.chain(command=command, timeout=timeout)
        else:
            comm = delegator.run(command=command, timeout=timeout)

        tpass = '<br><span class="{}"> >> {}</span>'.format(BTLogState.sysout,
                                                          str(comm.out).replace("\n", "<br>"))
        self.bti.add_log(tpass)
        retval = comm.return_code
        return retval

    def bt_raisestep(self):
        """
        raises step by 1

        :return:
        """
        self.bti.raisestep(1)

    def bt_isfinished(self, issuccess: bool = True):
        """
        sets finished state

        :param issuccess:
        :return:
        """
        self.bti.finished(issuccess)

    def get_thread_id(self)->int:
        sleep(1)
        return self.bti.id
