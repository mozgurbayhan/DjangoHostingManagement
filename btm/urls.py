# -*- coding: utf-8 -*-
from django.urls import path

from btm import views

__author__ = 'ozgur'
__creation_date__ = '23.04.2019' '23:01'
urlpatterns = [
    path('ati/', views.ajax_thread_info, name='ati'),
    path('getthreadinfo/<str:tid>', views.getthreadinfo, name='getthreadinfo'),
    path('testthreadinfo/', views.testthreadinfo, name='testthreadinfo'),
]
