# -*- coding: utf-8 -*-  
__author__ = 'ozgur'
__creation_date__ = '13.04.2019' '01:23'

from django.db import models


class Configdb(models.Model):
    confname = models.CharField(max_length=50, unique=True, verbose_name="Conf Name")
    configdict = models.TextField(verbose_name="Config Dict")

    created_at = models.DateTimeField(auto_now_add=True, verbose_name="Created At")
    updated_at = models.DateTimeField(auto_now=True, verbose_name="Updated At")

    class Meta:
        verbose_name = 'Config'
        verbose_name_plural = 'Config DB'
        ordering = ('-id',)

    def __str__(self):
        return str(self.confname)

    def get_short_name(self):
        return str(self.confname)
