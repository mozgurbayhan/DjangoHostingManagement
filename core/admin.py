# -*- coding: utf-8 -*-
from django.contrib import admin

from core.models import Configdb

__author__ = 'ozgur'
__creation_date__ = '16.04.2019' '21:59'


@admin.register(Configdb)
class ConfigdbAdmin(admin.ModelAdmin):
    list_display = ["id", "confname", "configdict"]


