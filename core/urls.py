# -*- coding: utf-8 -*-
from django.urls import path

from core import views

__author__ = 'ozgur'
__creation_date__ = '13.04.2019' '01:24'

urlpatterns = [
    path('logmein/', views.logmein, name='logmein'),
    path('logmeout/', views.logmeout, name='logmeout'),
    path('coreconfig/', views.coreconfig, name='coreconfig'),
    path('coreconfigdb/', views.coreconfig_db, name='coreconfig_db'),
]
