# -*- coding: utf-8 -*-  
__author__ = 'ozgur'
__creation_date__ = '13.04.2019' '01:22'

from django import forms


class LoginForm(forms.Form):
    username = forms.CharField(max_length=100, label="User", widget=forms.TextInput(
        attrs={'class': 'form-control', "placeholder": "User"}))
    password = forms.CharField(max_length=100, label="Password", widget=forms.PasswordInput(
        attrs={'class': 'form-control', "placeholder": "Password"}))


class DbPassForm(forms.Form):
    dbpass = forms.CharField(max_length=100, label="DB Password", required=False,
                             widget=forms.PasswordInput(
                                 attrs={'class': 'form-control', "placeholder": "Password"}))
    dbuser = forms.CharField(max_length=100, label="DB User", required=False,
                             widget=forms.TextInput(
                                 attrs={'class': 'form-control', "placeholder": "DBuser"}))
