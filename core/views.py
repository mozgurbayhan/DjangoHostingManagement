# -*- coding: utf-8 -*-

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.urls import reverse

from core.config import DbPassConfig
from core.forms import LoginForm, DbPassForm

__author__ = 'ozgur'
__creation_date__ = '13.04.2019' '01:23'

from django.contrib.auth import authenticate, login, logout
from django.shortcuts import redirect, render




def index(request):
    if not request.user.is_authenticated:
        return redirect(reverse("logmein"))
    if not request.user.is_active:
        logout(request)
        return redirect(reverse("logmein"))

    return render(request, "core/home.html", {})


def logmein(request):
    if request.method == 'POST':
        loginform = LoginForm(request.POST)
        if loginform.is_valid():
            username = loginform.cleaned_data['username']
            password = loginform.cleaned_data['password']
            user = authenticate(username=username, password=password)
            if user and user.is_active:
                login(request, user)
                return redirect(reverse("home"))
            else:
                messages.error(request, 'Username or password is wrong!')
        else:
            pass
    else:
        loginform = LoginForm()
    return render(request, "core/login.html", {"loginform": loginform})


@login_required
def logmeout(request):
    logout(request)
    return redirect(reverse("logmein"))

@login_required
def coreconfig(request):

    dbpc=DbPassConfig()
    dbpc.load()
    dbpf=DbPassForm(initial=dbpc.todict())

    return render(request, "core/config.html", {"dbpform": dbpf})


@login_required
def coreconfig_db(request):

    if request.method == 'POST':
        dbpf = DbPassForm(request.POST)
        if dbpf.is_valid():

            dbpc=DbPassConfig()
            dbpc.fromdict(dbpf.cleaned_data)
            dbpc.save()

    return redirect(reverse("coreconfig"))