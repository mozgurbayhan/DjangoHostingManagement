# -*- coding: utf-8 -*-
from DjangoHostingManagement.modulesettings import DHM_MODULES

__author__ = 'ozgur'
__creation_date__ = '17.04.2019' '11:34'


# noinspection PyUnusedLocal
def menu_items(request):
    retlist = []
    for key, moduleinfo in DHM_MODULES.items():
        retlist.append(moduleinfo)

    return {
        "menuitems": retlist
    }
