# Generated by Django 2.2 on 2019-07-03 11:08

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='configdb',
            name='modname',
        ),
        migrations.AddField(
            model_name='configdb',
            name='keyname',
            field=models.CharField(default=django.utils.timezone.now, max_length=50, unique=True, verbose_name='Key Name'),
            preserve_default=False,
        ),
    ]
