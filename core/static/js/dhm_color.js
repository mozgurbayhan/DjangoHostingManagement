const colorRed = '#dd4b39';
// noinspection JSUnusedGlobalSymbols
const colorYellow ="#f39c12";
const colorAqua ="#00c0ef";
const colorBlue = "#0073b7";
// noinspection JSUnusedGlobalSymbols
const colorBlack ="#111111";
// noinspection JSUnusedGlobalSymbols
const colorLightBlue ="#3c8dbc";
const colorGreen = '#00a65a';
// noinspection JSUnusedGlobalSymbols
const colorGray ="#d2d6de";
// noinspection JSUnusedGlobalSymbols
const colorNavy ="#001f3f";
// noinspection JSUnusedGlobalSymbols
const colorTeal ="#39cccc";
// noinspection JSUnusedGlobalSymbols
const colorOlive ="#3d9970";
// noinspection JSUnusedGlobalSymbols
const colorLime ="#01ff70";
const colorOrange ="#ff851b";
const colorFuchsia ="#f012be";
const colorPurple ="#605ca8";
// noinspection JSUnusedGlobalSymbols
const colorMaroon ="#d81b60";
const colorMuted ="#7a869d";