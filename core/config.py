# -*- coding: utf-8 -*-
import json

from core.models import Configdb

__author__ = 'ozgur'
__creation_date__ = '16.04.2019' '21:45'


class BaseConfig:
    def __init__(self, confname: str):
        self.confname = confname

    def save(self):
        config, iscreated = Configdb.objects.get_or_create(confname=self.confname)
        config.configdict = json.dumps(self.__dict__, ensure_ascii=False)
        config.save()

    def load(self):
        try:
            walkdict = self.__dict__.copy()
            configdict = json.loads(Configdb.objects.get(confname=self.confname).configdict,
                                    encoding="UTF-8")
            for key, value in walkdict.items():
                try:
                    self.__dict__[key] = configdict[key]
                except Exception:
                    pass

        except Exception:
            pass

    def todict(self) -> dict:
        return self.__dict__.copy()

    def fromdict(self, datadict: dict):
        self.__dict__.update(datadict.copy())


class DbPassConfig(BaseConfig):
    def __init__(self):
        super().__init__("dbpass")
        self.dbpass = None
        self.dbuser = None
