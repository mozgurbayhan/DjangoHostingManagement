# -*- coding: utf-8 -*-
from django.contrib import admin

from webappmanager.models import Webapp

__author__ = 'ozgur'
__creation_date__ = '01.07.2019' '13:52'

@admin.register(Webapp)
class ConfigdbAdmin(admin.ModelAdmin):
    list_display = [
        "appname",
        "url",
        "giturl",
        "dbname",
        "dbuser",
        "dbpass",
        "settingsfoldername",
        "staticfolder",
        "staticurl",
        "mediafolder",
        "mediaurl",
        "isinstalledsuccess",
    ]
