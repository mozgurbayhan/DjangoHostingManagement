# -*- coding: utf-8 -*-
import os

from DjangoHostingManagement import settings
from btm.backend_thread_manager import BackendThread
from core.config import DbPassConfig
from webappmanager.models import Webapp

import mysql.connector

__author__ = 'ozgur'
__creation_date__ = '10.07.2019' '15:33'

class BackupWebAppThread(BackendThread):
    def __init__(self, webapp: Webapp):
        self.webapp = webapp
        super().__init__(stepcount=1)


class DeleteWebAppThread(BackendThread):
    def __init__(self, webapp: Webapp):
        self.webapp = webapp
        super().__init__(stepcount=1)

    def delete_app(self):


        os.system("rm -rf /home/webapps/dbbackups/{}".format(self.webapp.appname))
        os.system("rm /etc/nginx/sites-enabled/{}".format(self.webapp.appname))
        os.system("rm -rf /etc/nginx/sites-available/{}".format(self.webapp.appname))
        os.system("systemctl restart nginx")

        os.system("systemctl stop webapp_{}".format(self.webapp.appname))
        os.system("systemctl disable webapp_{}".format(self.webapp.appname))
        os.system("rm /etc/systemd/system/webapp_{}.service".format(self.webapp.appname))

        os.system("rm -rf /home/webapps/apps/" + self.webapp.appname)
        os.system("rm -rf /home/webapps/venv/{}/".format(self.webapp.appname))

        dpc = DbPassConfig()
        dpc.load()
        mydb = mysql.connector.connect(host="localhost", user=dpc.dbuser, passwd=dpc.dbpass)
        mycursor = mydb.cursor()
        # noinspection SqlNoDataSourceInspection
        mycursor.execute("DROP DATABASE  {}".format(self.webapp.dbname))

    def backendthread(self):
        self.delete_app()
        self.bt_raisestep()


class AddWebAppThread(BackendThread):
    def __init__(self, webapp: Webapp, suuser: str = None, sumail: str = None, supass: str = None):
        self.webapp = webapp
        self.suuser = suuser
        self.sumail = sumail
        self.supass = supass

        self.apppath = "/home/webapps/apps/" + self.webapp.appname
        self.venvpath = "/home/webapps/venv/{}/".format(self.webapp.appname)
        self.pippath = "{}/bin/pip".format(self.venvpath)
        self.pythonpath = "{}/bin/python".format(self.venvpath)
        super().__init__(stepcount=7)

    def _rollback(self):
        self.bt_loginfo("Rolling back...")
        DeleteWebAppThread(self.webapp).delete_app()

    def _exc(self, comm: str):
        sysret = self.bt_execsys(comm, ischained=False)
        if sysret != 0:
            raise Exception("Execution of command failed with return code {}!".format(sysret))

    def _initialize(self):
        dpc = DbPassConfig()
        dpc.load()
        if not dpc.dbuser:
            raise Exception("Database user is not set in DHM settings")
        if not dpc.dbpass:
            raise Exception("Database password is not set in DHM settings")

        if os.path.exists(self.apppath):
            raise Exception("Application path already exists")

        if os.path.exists(self.venvpath):
            raise Exception("Virtual environment already exists")

    def _create_db(self):
        dpc = DbPassConfig()
        dpc.load()
        mydb = mysql.connector.connect(host="localhost", user=dpc.dbuser, passwd=dpc.dbpass)
        mycursor = mydb.cursor()
        # noinspection SqlNoDataSourceInspection
        q = "CREATE DATABASE IF NOT EXISTS {} CHARACTER SET utf8 COLLATE utf8_general_ci;".format(
            self.webapp.dbname)
        mycursor.execute(q)

    def _create_venv(self):

        self._exc("virtualenv -p /usr/bin/python3 " + self.venvpath)
        self._exc("chown webapps:webapps -R " + self.venvpath)

    def _create_project(self):
        self._exc("sudo -H -u webapps git clone {} {}".format(self.webapp.giturl, self.apppath))
        self._exc("sudo -H -u webapps {} install -r {}/requirements.txt".format(self.pippath,
                                                                                self.apppath))
        self._exc("sudo -H -u webapps {} install gunicorn".format(self.pippath))
        self._exc("sudo -H -u webapps {} install mysqlclient".format(self.pippath))

        if self.webapp.localsettings:
            targetpath = "{}/{}/local_settings.py".format(self.apppath,
                                                          self.webapp.settingsfoldername)
            with open(targetpath, "w+") as openfile:
                openfile.write(self.webapp.localsettings)

        self._exc("chown -R webapps:webapps {}".format(self.apppath))
        self._exc("chown webapps:webapps {}".format(self.venvpath))
        self._exc(
            "sudo -H -u webapps {} {}/manage.py migrate".format(self.pythonpath, self.apppath))

        comm = "sudo -H -u webapps {} {}/manage.py shell -c \"from " \
               "django.contrib.auth.models import User; " \
               "User.objects.create_superuser('{}', '{}', '{}')\"".format(
            self.pythonpath, self.apppath, self.suuser, self.sumail, self.supass)
        self._exc(comm)

        comm = "sudo -H -u webapps {} {}/manage.py collectstatic --noinput".format(
            self.pythonpath, self.apppath)
        self._exc(comm)

    def _create_gunicorn(self):

        targetpath = "/etc/systemd/system/webapp_{}.service".format(self.webapp.appname)
        helperpath = os.path.join(settings.BASE_DIR, "webappmanager", "helpersh",
                                  "gunicorn.service")
        with open(helperpath, "r") as openfile:
            temp = openfile.read()
        temp = temp.replace("[APPNAME]", self.webapp.appname)
        temp = temp.replace("[SETTINGSFOLDER]", self.webapp.settingsfoldername)
        with open(targetpath, "w+") as openfile:
            openfile.write(temp)

        self._exc("systemctl enable webapp_{}".format(self.webapp.appname))
        self._exc("systemctl start webapp_{}".format(self.webapp.appname))

    def _create_nginx(self):
        targetpath = "/etc/nginx/sites-available/{}".format(self.webapp.appname)
        helperpath = os.path.join(settings.BASE_DIR, "webappmanager", "helpersh", "nginx.available")
        with open(helperpath, "r") as openfile:
            temp = openfile.read()
        temp = temp.replace("[APPNAME]", self.webapp.appname)
        temp = temp.replace("[URL]", self.webapp.url)
        with open(targetpath, "w+") as openfile:
            openfile.write(temp)

        self._exc("ln -s /etc/nginx/sites-available/{} /etc/nginx/sites-enabled".format(
            self.webapp.appname))

        self._exc("nginx -t")

        self._exc("systemctl restart nginx")

    def _finalize(self):
        self._exc("sudo -H -u webapps mkdir /home/webapps/dbbackups/{}".format(self.webapp.appname))

    def backendthread(self):
        self.bt_logheader("Installing : " + self.webapp.appname)
        try:

            self.bt_logheader(">> Checking errors...")
            self._initialize()
            self.bt_raisestep()
            self.bt_logheader(">> Creating db")
            self._create_db()
            self.bt_raisestep()
            self.bt_logheader(">> Creating venv")
            self._create_venv()
            self.bt_raisestep()
            self.bt_logheader(">> Creating project folder")
            self._create_project()
            self.bt_raisestep()
            self.bt_logheader(">> Setting up gunicorn")
            self._create_gunicorn()
            self.bt_raisestep()
            self.bt_logheader(">> Setting up nginx")
            self._create_nginx()
            self.bt_raisestep()
            self.bt_logheader(">> Finalizing...")
            self._finalize()
            self.bt_raisestep()
        except Exception as ex:
            self.bt_logexception(ex, "Test Exception raised")
            self._rollback()
        return
