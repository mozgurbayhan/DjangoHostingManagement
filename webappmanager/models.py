# -*- coding: utf-8 -*-
from django.db import models

__author__ = 'ozgur'
__creation_date__ = '01.07.2019' '13:53'


class Webapp(models.Model):
    appname = models.CharField(max_length=50, verbose_name="Application Name", unique=True)
    url = models.URLField(max_length=250, verbose_name="URL")
    giturl = models.CharField(max_length=250, verbose_name="Git URL")
    dbname = models.CharField(max_length=50, verbose_name="DB Name")
    dbuser = models.CharField(max_length=50, verbose_name="DB User")
    dbpass = models.CharField(max_length=50, verbose_name="DB Pass")
    settingsfoldername = models.CharField(max_length=50, verbose_name="Settings Folder Path")
    staticfolder = models.CharField(max_length=250, default="static/",verbose_name="Static Folder Path")
    staticurl = models.CharField(max_length=50, default="/static/",verbose_name="Static URL")
    mediafolder = models.CharField(max_length=250, default="media/", verbose_name="Media Folder Path")
    mediaurl = models.CharField(max_length=50, default="/media/",verbose_name="Media URL")
    isinstalledsuccess = models.BooleanField(default=False, blank=True,
                                             verbose_name="Is Installed Succesfully")

    localsettings=models.TextField(null=True, blank=True, verbose_name="local_settings.py content")

    created_at = models.DateTimeField(auto_now_add=True, verbose_name="Created At")
    updated_at = models.DateTimeField(auto_now=True, verbose_name="Updated At")

    class Meta:
        verbose_name = 'Webapp'
        verbose_name_plural = 'Webapps'
        ordering = ('-id',)

    def __str__(self):
        return str(self.appname)

    def get_short_name(self):
        return str(self.appname)
