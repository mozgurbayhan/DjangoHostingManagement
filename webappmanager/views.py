# -*- coding: utf-8 -*-
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.urls import reverse

from webappmanager.backendtasks import AddWebAppThread, DeleteWebAppThread
from webappmanager.forms import WebAppForm
from webappmanager.models import Webapp

__author__ = 'ozgur'
__creation_date__ = '01.07.2019' '13:53'


@login_required
def getwalist(request):
    return render(request, "wam/wam.html", {"webapps":Webapp.objects.all()})


@login_required
def addwa(request):
    if request.method == 'POST':
        waform = WebAppForm( request.POST)
        if waform.is_valid():
            webapp = waform.save()
            suuser=request.POST.get("ext_suname", None)
            sumail=request.POST.get("ext_sumail", None)
            supass=request.POST.get("ext_supass", None)

            addwathread = AddWebAppThread(webapp,suuser=suuser,sumail=sumail,supass=supass)
            addwathread.start()

            retdict = {
                "tid": addwathread.get_thread_id()
            }
        else:
            retdict = {
                "waform": waform
            }

    else:
        retdict = {
            "waform": WebAppForm()

        }
    return render(request, "wam/addwa.html", retdict)


@login_required
def rmwa(request,appid):
    webapp =Webapp.objects.get(id=appid)
    deletewathread = DeleteWebAppThread(webapp)
    deletewathread.start()
    tid=deletewathread.get_thread_id()
    return redirect(reverse("getthreadinfo", args=[tid]))