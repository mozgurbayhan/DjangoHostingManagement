# -*- coding: utf-8 -*-
from django import forms

from webappmanager.models import Webapp

__author__ = 'ozgur'
__creation_date__ = '01.07.2019' '14:14'


class WebAppForm(forms.ModelForm):
    class Meta:
        model = Webapp
        exclude = ["isinstalledsuccess"]
        widgets = {
            'appname': forms.TextInput(attrs={"class": "form-control", "placeholder": "Unique application name"}),
            'url': forms.URLInput(attrs={"class": "form-control", "placeholder": "Global URL of the application"}),
            'giturl': forms.TextInput(attrs={"class": "form-control", "placeholder": "Source code repo of the application"}),
            'dbname': forms.TextInput(attrs={"class": "form-control", "placeholder": "db name in settings.py"}),
            'dbuser': forms.TextInput(attrs={"class": "form-control", "placeholder": "db user in settings.py"}),
            'dbpass': forms.PasswordInput(attrs={"class": "form-control", "placeholder": "db password in settings.py"}),
            'settingsfoldername': forms.TextInput(
                attrs={"class": "form-control", "placeholder": "settings.py's folder name relative to project folder"}),
            'staticfolder': forms.TextInput(attrs={"class": "form-control", "placeholder": "static folder name relative to project folder e.g: static/"}),
            'staticurl': forms.TextInput(attrs={"class": "form-control", "placeholder": "static url relative to project url e.g: /static/"}),
            'mediafolder': forms.TextInput(attrs={"class": "form-control", "placeholder": "media folder name relative to project folder e.g: media/"}),
            'mediaurl': forms.TextInput(attrs={"class": "form-control", "placeholder": "media url relative to project url e.g: /media/"}),
            'localsettings': forms.Textarea(attrs={"class": "form-control", "placeholder": "optional: local_settings.py content"}),
        }
