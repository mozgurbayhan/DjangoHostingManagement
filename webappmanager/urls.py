# -*- coding: utf-8 -*-
from django.urls import path

from webappmanager import views

__author__ = 'ozgur'
__creation_date__ = '01.07.2019' '13:53'

urlpatterns = [
    path('walist/', views.getwalist, name='getwalist'),
    path('addwa/', views.addwa, name='addwa'),
    path('rmwa/<int:appid>', views.rmwa, name='rmwa'),
]
